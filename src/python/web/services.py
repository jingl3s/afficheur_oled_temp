#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Fichiers contenant les liens de la page web
'''
from bottle import request, get, post
from bottle import static_file, template
import os
import logging 

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

__author__ = 'zorbac at free.fr'
__copyright__ = 'WTPL'
__license__ = 'WTPL'

dict_image_str = list()
g_list_sensor = list()

g_template = "page.tpl"

interact = None

fct_return_service = None

logger = None

# ######### BUILT-IN ROUTERS ###############


@get('/__exit')
@post('/__exit')
def __exit():
    global server
    server.stop()


@get('/assets/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='/sdcard')


# @get('/img/<filepath:path>')
# def server_static2(filepath):
#     imag = create_image(128, 64)
#     fichier = os.path.join(os.path.dirname(__file__),
#                            "ressources", "image.png")
#     imag.save(fichier, format="PNG")
#     return static_file(fichier, root='/')


@get('/sensor/<temp:float>&<temp2:float>')
def _play_sound(temp, temp2):
    print("coucou {},{}".format(temp, temp2))

    return fct_return_service()


@post('/get_valeurs')
def _get_valeurs():
    global interact
    list_vals = interact.get_valeurs_temp()
    dict_val = dict()
    for index, vals in enumerate(list_vals):
        dict_val['{}'.format(index)] = vals
    return dict_val


@post('/lit_valeurs')
def _lit_valeurs():
    global interact
    interact.lit_valeurs()
    return _get_valeurs()


@post('/text')
def _text():
    global interact
    global logger
    text = request.forms.getall('text')
    add = request.forms.getall('add')
    mod = request.forms.getall('mod')
    reset = request.forms.getall('reset')
    index = request.forms.getall('index')
    logger.debug("=+=+=+=+=+=+{},{},{},{}, {} =+===+=+".format(text,add,mod,reset,index))
    if len(text) == 0:
        logger.error("Invalid input text is empty")
        used_text = "ERR."
    else:
        used_text = text[0]
    
    if len(reset) > 0 and "reset" in reset[0]:
        interact.reset_text()

    if len(add) > 0 and  "add" in add[0]:
        interact.add_text(used_text)

    if len(mod) > 0 and "mod" in mod[ 0] :
        if len(index) != 0:
            interact.add_text(used_text, int(index[0]) )

    return fct_return_service()


@post('/sensor2')
def _sensor2():
    global interact
    global g_list_sensor
    temp = request.forms.getall('temperature')[0]
    humidite = request.forms.getall('humidite')[0]
    index = request.forms.getall('index')
    reset = list()
    add = request.forms.getall('add')
    mod = request.forms.getall('mod')

    if len(reset) > 0 and reset[0] in "reset_temp":
        interact.reset_valeurs_temp()

    if len(add) > 0 and add[0] in "add":
        interact.add_valeurs_temp(temp, humidite)

    if len(mod) > 0 and mod[0] in "mod":
        interact.add_valeurs_temp(temp, humidite, int(index[0]))

    g_list_sensor[0] = temp
    g_list_sensor[1] = humidite
    g_list_sensor[2] = int(index[0])

    return fct_return_service()


@post('/mise_a_jour_images')
def mise_a_jour_images():
    global dict_image_str
    global interact
    interact.display_text()

    return fct_return_service()

######### WEBAPP ROUTERS ###############


@get('/')
def home():
    global interact
    global dict_image_str
    global g_list_sensor
    global g_template

    dict_image_str = interact.get_images_base64()

    return template(os.path.join((os.path.split(__file__))[0], 'ressources', g_template),
                    name='QPython',
                    image_base64=dict_image_str,
                    temp_humi=g_list_sensor,
                    list_text=interact.get_text(),
                    nombre_capteurs=interact.get_nombre_temp(),
                    list_text_affichage=interact.get_displayed_text())


def home_rien():
    return {"Success": True}


######### WEBAPP ROUTERS ###############


def set_interraction(p_interact):
    global interact
    interact = p_interact


def set_configuration(configuration):
    global fct_return_service
    global g_template
    global interact
    if configuration['retour_home'] == 1:
        fct_return_service = home
    else:
        fct_return_service = home_rien

    if "template" in configuration:
        g_template = configuration["template"]

    if len(g_list_sensor) == 0:
        # initialise valeurs par defaut
        valeurs = interact.get_valeurs_temp(0)

        if valeurs is not None:
            val_sensor = valeurs[0]
            g_list_sensor.append(val_sensor['temp'])
            g_list_sensor.append(val_sensor['humidity'])
            g_list_sensor.append(0)
    global logger
    logger =logging.getLogger("hugo")