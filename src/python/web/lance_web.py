#-*-coding:utf8;-*-
# qpy:3
# qpy:webapp:sound player web Qpython
# qpy://127.0.0.1:8081/
import os
from common.fonctions_systeme import get_adresse_ip
'''
@author: zorbac

This is based on sample for qpython webapp
'''
# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).
# sys.path.append('/data/data/com.termux/files/usr/lib/python3.6/site-packages/')

import bottle

try:
    from web import services
    from web.server import MyWSGIRefServer
except Exception as e:
    print(e)


__author__ = 'zorbac at free.fr'
__copyright__ = 'WTPL'
__license__ = 'WTPL'

######### QPYTHON WEB SERVER ###############



def lance_serveur(configuration, interaction, log_level: str="DEBUG"):
    services.set_interraction(interaction)
    services.set_configuration(configuration['service'])

    app = bottle.default_app()

    try:
        ip_address = '0.0.0.0'
        server = MyWSGIRefServer(host=ip_address,  port=configuration['port'])

        reload_page = True
        debug_enabled = False

        # Settings for Operational usage or development purpose
        if log_level == "WARNING" or log_level == "ERROR":
            server.quiet = True
        else:
            # reloader permet de recharger des fichiers template modifie lors processus execution
            # recommende de le changer en prod
            reload_page = False

            # For development purpose
            debug_enabled=True

        #app.run(server=server, reloader=reload_page, debug=debug_enabled)
        app.run(server='paste', host=ip_address, port=configuration['port'])

    except (Exception) as ex:
        print("Exception: %s" % repr(ex))



if __name__ == "__main__":
    print("Non disponible")
