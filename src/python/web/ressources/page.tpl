
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<!-- sources : 
    http://www.w3schools.com/tags/att_meta_http_equiv.asp 
    http://codesupport.info/6-important-features-as-how-to-do-it-using-meta-tags/
    -->
<meta http-equiv="Cache-Control" content="no-store">
<meta charset="utf-8">
<title>Hugo Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="Hugo Carnide" name="author">

</head>
<body>
<style>
.column-left {
	float: left;
	width: 33%;
}

.column-right {
	float: right;
	width: 33%;
}

.column-center {
	display: inline-block;
	width: 33%;
}

button.test {
	font-size: 5rem;
	height: auto;
	width: auto;
}

.disp {
	font-size: 3rem;
	height: auto;
	width: auto;
}

.disp_w {
	font-size: 3rem;
	text-align: center;
	height: auto;
	width: 30vw;
	}

.disp_h {
	font-size: 3rem;
	text-align: center;
	height: 3rem;
	width: 30vw;
	}
</style>

<h1>Afficheur information en cours !</h1>

<form method="get" action="/">
    <button type="submit" class="disp">Principal</button>
</form>
<fieldset>
<legend class="disp">
Capteur
</legend>

<form method="post" action="/sensor2">
    <a class="disp_w" >Temperature</a>
    <a class="disp_w" >Humidite</a>
    <BR/>
    %if nombre_capteurs != 0: 
    %hum_val='{:0.1f}'.format(float(temp_humi[1]))
    <input type="numeric" name="temperature" value="{{temp_humi[0]}}"  class="disp_w">
    <input type="numeric" name="humidite" value="{{hum_val}}" class="disp_w">
    %else:
    <input type="numeric" name="temperature" value=""  class="disp_w">
    <input type="numeric" name="humidite" value="" class="disp_w">
    %end

    <BR/>
    %if nombre_capteurs != 0: 
    %for index in range(nombre_capteurs):
    <input type="radio" name="index" id="{{index}}" value="{{index}}" {{!'checked="checked" ' if index == temp_humi[2] else ""}} class="disp_h">
    {{index}}
    %end 
    %else:
    <input type="radio" name="index" id="0" value="0" checked="checked" class="disp">0</input>
    %end
    <BR/>
    %if nombre_capteurs != 0: 
    <button type="submit" id="modify_temp" name="mod" value="mod" class="disp" >Modifie valeurs</button>
    %end
    <button type="submit" id="add_temp" name="add" value="add" class="disp">Ajout valeurs</button>
</form>
</fieldset>
<fieldset>
<legend class="disp">
Texte
</legend>

<form method="post" action="/text">
    <input name="text" value="" class="disp_w">
    <br />
    <!-- 	% print("<p>{}</p>".format(text_line))  -->
    % for text_line in list_text:   
        {{text_line}}<br />
    % end
    <button id="id_add" name="add" type="submit" class="disp" value="add">Ajoute text</button>
    <button id="id_reset" name="reset" type="submit" class="disp" value="reset">Reset text</button>
</form>
</fieldset>

<fieldset>
<legend class="disp">
Affichage
</legend>

<form action="/mise_a_jour_images" method="post">
    <input type="submit" value="Mise à jour" class="disp" />
</form>

    % for text_line in list_text_affichage:   
        {{text_line}}<br />
    % end


% counter = 0 
<table>
% for image_titre, image_base64_unique in image_base64.items(): 

% if (counter % 3) == 0:
<TR>
%end 
% counter += 1
<TD>
{{image_titre}}
</TD>
<TD>
<img alt="Embedded Image" src="data:image/png;base64,{{image_base64_unique}}" />
</TD>
% if (counter % 3) == 0:
</TR>
%end 
%end
</table>

</fieldset>



<fieldset>
<legend class="disp">
REST request
</legend>


<form method="get" action="/sensor/26.5&55.2">
    <button type="submit" class="disp">Sensor</button>
</form>


<form method="post" action="/get_valeurs">
    <button type="submit" class="disp">Get valeurs</button>
</form>

<form method="post" action="/lit_valeurs">
    <button type="submit" class="disp">Lit valeurs</button>
</form>

</fieldset>


<br />
<br />
<a href="javascript:milib.close()">>> Exit</a>
<!-- <br /> -->
<!-- <a href="http://wiki.qpython.org/doc/program_guide/web_app/">>> -->
<!-- 	About QPython Web App</a> -->
<!-- <br /> -->
<!-- <a href="/play?id=0">>> play</a> -->
<!-- <br /> -->
<!-- <a href="/_play/0">>> play</a> -->
<!-- <br /> -->

<!-- <a href="/__ping">>> Ping</a> -->

</body>