#-*-coding:utf8;-*-
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).


from common import fonctions_systeme
import main_lance_web as setting
import urllib.request
from urllib.error import URLError
from time import sleep
import sys

TEMPS_ENTRE_RAFRAICHISSEMENT = 600

def lance_rafraichir_page():
    '''
    Rafraichi l'URL du serveur pour mettre à jour l'affichage
    '''
    if 1 < len(sys.argv):
        fichier_configuration = setting.get_config(sys.argv[1])
    else:
        fichier_configuration = setting.get_config()

    if 3 == len(sys.argv):
        temps_attente = float(sys.argv[2])
    else:
        temps_attente = TEMPS_ENTRE_RAFRAICHISSEMENT

    logger, config_json = setting.configure(
        config_select=fichier_configuration)
    port_reseau = "8082"
    if "WEB" in config_json:
        if "port" in config_json["WEB"]:
            port_reseau = config_json["WEB"]["port"]

    ip_address = fonctions_systeme.get_adresse_ip()

    url_update = "http://{}:{}/sensor2".format(
        ip_address, port_reseau)

    url_update_text = "http://{}:{}/text".format(
        ip_address, port_reseau)

    # Ajout valeurs pour être sûr d'utiliser un POST
    values1 = {
        "temperature": "10",
        "humidite": "50",
        "mod": "mod",
        "index": "0"
    }
    values2 = {
        "temperature": "-20",
        "humidite": "20",
        "mod": "mod",
        "index": "1"
    }
    list_value1 = {'temp_start': 20, 'temp_end': 0, 'temp_pas': -
                   0.5, 'hum_start': 50, 'hum_end': 20, 'hum_pas': -0.5}
    list_value2 = {'temp_start': -20, 'temp_end': 1,
                   'temp_pas': 0.5, 'hum_start': 20, 'hum_end': 80, 'hum_pas': 1}
    list_text = ["Bonjour", "Allo", "Ola", "Bom dia",
                 "Hello", "Au revoir", "A+", "Ate a proxima", "Bye"]
    
    list_value_key =list()
    list_value_key.append(['temperature', "temp_start",'temp_end','temp_pas'])
    list_value_key.append(['humidite', "hum_start",'hum_end','hum_pas'])
    
    values_text = {
        "text": list_text[0],
        "add": "add",
        "mod": ""
    }

    list_valeurs_capteurs_temp = [
        [values1, list_value1], [values2, list_value2]]

    boucle_infinie = True
    index = - 1
    while boucle_infinie:

        data1 = urllib.parse.urlencode(values1)
        # data should be bytes
        data1 = data1.encode('ascii')

        data2 = urllib.parse.urlencode(values2)
        # data should be bytes
        data2 = data2.encode('ascii')

        index = (index + 1) % len(list_text)
        values_text["text"] = list_text[index]
        data_text = urllib.parse.urlencode(values_text)
        # data should be bytes
        data_text = data_text.encode('ascii')

        try:

            with urllib.request.urlopen(url_update, data1) as response:
                html = response.read()
                if "error" in str(html).lower():
                    boucle_infinie = False
                # Alternative pour avoir la réponse json
                #         result = json.loads(response.readall().decode('utf-8'))

            with urllib.request.urlopen(url_update, data2) as response:
                html = response.read()
                if "error" in str(html).lower():
                    boucle_infinie = False

            with urllib.request.urlopen(url_update_text, data_text) as response:
                html = response.read()
                if "error" in str(html).lower():
                    boucle_infinie = False

            sleep(temps_attente)

        except URLError as e:
            boucle_infinie = False
            logger.error("Exception", e)

        for value, list_value in list_valeurs_capteurs_temp:
            
            for keys in list_value_key:
                if abs(float(value[keys[0]]) - list_value[keys[2]]) == 0:
                    new_val = list_value[keys[1]]
                else:
                    new_val = float(value[keys[0]]) + list_value[keys[3]]
                value[keys[0]] = str(new_val)
            
    logger.exception("URL rafraichissement à renvoyé une erreur")


def fonctions_tests():

    #     url = "http://{}:8081".format(ip_address)
    #     with urllib.request.urlopen(url) as response:
    #         html = response.read()
    #     print(html)

    #     import requests
    #     response = requests.post(url_update)
    #     print(response.text)
    pass


if __name__ == '__main__':
    lance_rafraichir_page()
