#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).


import os
import sys

from interaction.controler import Controler
from oled.oled_dummy import OledDummy
from oled.oled_i2c_communication import OledI2cCommunication
from oled.preparation_contenu_ecran_oled import PreparationContenuEcranOled
from web import lance_web
from tools import workbench

CONFIG_RPI_OLED = "config_rpi_oled.json"
CONFIG_RPI_FULL = "config_rpi_oled_dht.json"
CONFIG_FULL = "config_full.json"
CONFIG_FULL_REST = "config_full_rest.json"


def affiche_image(disp_image):
    # Creation image
    height = 64
    ligne_text = list()
    ligne_text.append("24.6° 56%")
    ligne_text.append("-12.1° 48%")
    for cpt in range(0, 6):
        _ecran = PreparationContenuEcranOled()
        _ecran.set_heigth(height)
        _ecran.set_width(128)
        _ecran.set_font_index(cpt)
        for _text in ligne_text:
            _ecran.add_text(_text)

        imag = _ecran.get_image()
        if disp_image:
            #     oled.display_img(imag)
            imag.show("Test")


def get_config(config_force=""):
    if "" != config_force:
        config_file = config_force
    else:
        import socket

        hostname = socket.gethostname()
        if hostname in "raspberrypi":
            print("Rpi detecte")
            config_file = CONFIG_RPI_FULL
#             config_file=CONFIG_RPI_OLED
        else:
            config_file = CONFIG_FULL
#             config_file=CONFIG_FULL_REST
    return config_file


def lance(config_file, root_dir):
    _logger, _config_json = workbench.configure(config_file, root_dir)

    _repertoire_polices = os.path.join(root_dir,  'fonts')
    _repertoire_polices = os.path.realpath(_repertoire_polices)
    cont = os.listdir(_repertoire_polices)
#    _logger.debug('dir:{}'.format(_repertoire_polices))
    list_font = list()
    for fichier in cont:
        if fichier.lower().endswith('ttf'):
            fichier_ttf = os.path.join(_repertoire_polices, fichier)
            list_font.append(fichier_ttf)

    interact = Controler.get_interraction(
        _config_json['INTERACTION'], list_font)

 #   _logger.debug("config_file: %s", config_file)

    # OLED communication
    if "ECRAN" in _config_json:
        oled = OledI2cCommunication()
        address = int(_config_json["ECRAN"]["i2c_address"], 16)
        oled.set_config(_config_json["ECRAN"]["gpio_pin_rst"],
                        _config_json["ECRAN"]["i2c_bus"], address)
        oled.initialize()
    else:
        oled = OledDummy()
    interact.set_oled(oled)

    # DHT
    list_dht = list()
    if "DHT" in _config_json:
        from dht.dht_lecture import DhtLecteur
        for dht in _config_json["DHT"]:
            o_dht = DhtLecteur(dht['gpio_pin'], dht['sensor'])
            list_dht.append(o_dht)
        interact.set_dht(list_dht)
        interact.lit_valeurs()
    else:
        if "EXEMPLE" in _config_json:
            if "TEMPERATURE" in _config_json["EXEMPLE"]:
                for donne_temp in _config_json["EXEMPLE"]["TEMPERATURE"]:
                    temp = 0
                    if "temp" in donne_temp:
                        temp = donne_temp["temp"]
                    hum = 0
                    if "humidite" in donne_temp:
                        hum = donne_temp["humidite"]
                    interact.add_valeurs_temp(temp, hum)
        pass

    if "EXEMPLE" in _config_json:
        if "TEXTE" in _config_json["EXEMPLE"]:
            for text in _config_json["EXEMPLE"]["TEXTE"]:
                interact.add_text(text)

    # Force le premier affichage dès le démarrage
#    interact.display_text()

    lance_web.lance_serveur(_config_json["WEB"], interact)



if __name__ == "__main__":
    fichier_config, dir_rsrc = workbench.retrieve_inputs()
    if "" == fichier_config:
        fichier_config = get_config()
    lance(fichier_config, dir_rsrc)
