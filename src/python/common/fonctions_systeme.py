#-*-coding:utf8;-*-
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os


def get_adresse_ip():
    ip_address_read = os.popen('ip addr show')\
        .read()
   # print ('ip : {}'.format(ip_address_read))

    ip_address = '127.0.0.1'

    if not ip_address_read in "":

        list_ip_address = ip_address_read.split("inet ")
        #[1]\
        #   .split("/")
     #   print(list_ip_address)
      #  print(len(list_ip_address))
        ip_address = list_ip_address[len(list_ip_address) - 1].split('/')[0]

    # stack overflow 14958597
    ip_address = '0.0.0.0'

#     print ('ip : {}'.format(ip_address))

    return ip_address


if __name__ in '__main__':
    print('sdddd')
    get_adresse_ip()
