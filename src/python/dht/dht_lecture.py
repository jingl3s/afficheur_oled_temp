#!/usr/bin/python

# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import Adafruit_DHT
import logging


class DhtLecteur(object):
    def __init__(self, pin=17, sensor=22):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.debug("---------------")

        # Sensor should be set to Adafruit_DHT.DHT11,
        # Adafruit_DHT.DHT22, or Adafruit_DHT.AM2302.
        if sensor == 22:
            self._sensor = Adafruit_DHT.DHT22
        elif sensor == 11:
            self._sensor = Adafruit_DHT.DHT11
        elif sensor == 23:
            self._sensor = Adafruit_DHT.AM2302
        else:
            raise ValueError('invalid sensor dht')

        # Example using a Raspberry Pi with DHT sensor
        # connected to GPIO23.
        self._pin = pin

        self._valeurs = list()
        self._valeurs.append(0)
        self._valeurs.append(0)

    def obtenir_infos(self):
        # Try to grab a sensor reading.  Use the read_retry method which will retry up
        # to 15 times to get a sensor reading (waiting 2 seconds between each
        # retry).
        humidity, temperature = Adafruit_DHT.read_retry(
            self._sensor, self._pin)
        self._valeurs[0] = self._convert_to_string(temperature)
        self._valeurs[1] = self._convert_to_string(humidity)
        self._logger.debug("Infos: %s", self._valeurs)
        return self._valeurs

    def affiche_valeurs(self):
        self.obtenir_infos()
        temp = self._valeurs[0]
        humidity = self._valeurs[1]

        s_temperature = self._convert_to_string(temp)
        s_humidity = self._convert_to_string(humidity)

        # Note that sometimes you won't get a reading and
        # the results will be null (because Linux can't
        # guarantee the timing of calls to read the sensor).
        # If this happens try again!

        self._logger.info("Temp={}*C  Humidity={}%".format(s_temperature, s_humidity))

    def _convert_to_string(self, fl_val):
        if fl_val is not None:
            s_val = "{0:0.1f}".format(fl_val)
        else:
            s_val = "ERR"
        return s_val


if __name__ == '__main__':
    dht = DhtLecteur(17, sensor=22)
