#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os

from rpi.configuration_rpi import ConfigurationRpi
from rpi.rpi_boutons2 import RpiBoutons
import logging


_logger = None


def lance(p_dossier_config_rpi: str = "", p_nom_fichier_config_rpi: str = ""):
    global _logger
    _logger = logging.getLogger(__package__)

    # Configuration
    if p_dossier_config_rpi in "":
        dossier_config = os.path.join(os.path.dirname(__file__),
                                      "..",
                                      "configs",
                                      "rpi")
    else:
        dossier_config = p_dossier_config_rpi

    if p_nom_fichier_config_rpi in "":
        path_config = os.path.join(dossier_config,
                                   "config_rpi.json")
    else:
        path_config = os.path.join(dossier_config,
                                   p_nom_fichier_config_rpi)

    rpi_config = ConfigurationRpi(path_config)

    json_config_rpi = rpi_config.get_config()
    btn_appuie = None
    if "appuie" in json_config_rpi:
        btn_appuie = json_config_rpi["appuie"]

    btn_relache = None
    if "relache" in json_config_rpi:
        btn_relache = json_config_rpi["relache"]

    # RaspberryPi
    rpi = RpiBoutons(cmd=fct_cmd_eteindre_ordi, cmd_next=fct_cmd_next_vide)
    rpi.charge_config(btn_appuie, btn_relache=btn_relache)

    rpi.run()


def fct_cmd_eteindre_ordi(p_val_param):
    global _logger
    _logger.info('extinction cours')
    _logger.debug('Parametre: %s', p_val_param)


def fct_cmd_next_vide():
    _logger.info("Fonction vide")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    lance()
