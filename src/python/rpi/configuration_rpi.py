#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os

from common.configuration_loader import ConfigurationLoader


class ConfigurationRpi(object):
    '''
    classdocs
    '''

    def __init__(self, path_json_file):
        '''
        Constructor
        '''
        DOSSIER_CONFIGS = os.path.join(os.path.dirname(__file__),
                                       "..",
                                       "configs", "rpi")

        self._dossier_fichier_config = (os.path.split(path_json_file))[0]
        file = (os.path.split(path_json_file))[1]
        gestion_config = ConfigurationLoader(
            self._dossier_fichier_config)
        gestion_config.set_configuration_file_name(file)
        gestion_config.set_chemin_configuration_default(
            os.path.dirname(__file__))
        self._config_json = gestion_config.obtenir_configuration()

    def get_config(self):
        return self._config_json


if __name__ == '__main__':
    try:
        fichier = os.path.join((os.path.dirname(__file__)),
                               'config_rpi.json')
        config = ConfigurationRpi(fichier)
        elements = config.get_config()
        print(elements)
    except Exception as e:
        print(e)
