#-*-coding:utf8;-*-
'''
@author: zorbac
'''

# license
#
# Producer 2018 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import time
import os
import sys
# import socket
import subprocess


if __name__ == '__main__':
    dossier_src = os.getcwd()
    dossier = os.path.dirname(__file__)
    dossier = os.path.realpath(dossier)
    os.chdir(dossier)
    sys.path.append(dossier)

#     hostname = socket.gethostname()
#     if hostname in "localhost":
#         if sys.version.split(" ")[0] in "3.6.1":
#             python_cmd = "/data/user/0/org.qpython.qpy3/files/bin/qpython-android5.sh"
#         else:
#             python_cmd = "python"
#     else:
#         python_cmd = "python3"
    # Reuse same python command as current as current script
    python_cmd = sys.executable

    print('argv: {}'.format(sys.argv))
    list_cmd_lot1 = list()
    list_cmd_lot2 = list()

    if 1 < len(sys.argv):
        if "demo" == sys.argv[1].lower():
            temps_attente = "1"
            list_cmd_lot1.append(
                [python_cmd, "lanceurs/main_lance_web.py", "config_affiche_demo.json"])
            list_cmd_lot2.append(
                [python_cmd, "lanceurs/main_rafraichi_affichage.py", "config_affiche_demo.json", temps_attente])
            list_cmd_lot2.append(
                [python_cmd, "lanceurs/main_simul_donnees.py", "config_affiche_demo.json", temps_attente])

        if "rpi" == sys.argv[1].lower():
            list_cmd_lot1.append([python_cmd, "lanceurs/main_lance_web.py"])
            list_cmd_lot1.append([python_cmd, "lanceurs/main_lance_rpi_actions.py"])
            list_cmd_lot2.append([python_cmd, "lanceurs/main_rafraichi_affichage.py"])

        if "bus" == sys.argv[1].lower():
            temps_attente = "1"
            list_cmd_lot1.append(
                [python_cmd, "lanceurs/main_lance_web.py", "config_oled_bus.json"])
#             list_cmd_lot2.append(
 #               [python_cmd, "main_simul_donnees.py", "config_oled_bus.json", temps_attente])
#                 [python_cmd, os.path.join("lanceurs","simul_dht.py"), "config_oled_bus.json", temps_attente])
#            list_cmd_lot2.append([python_cmd, "main_rafraichi_affichage.py"])

    if 0 == len(list_cmd_lot1):
        list_cmd_lot1.append([python_cmd, "lanceurs/main_lance_web.py"])



    list_popen = list()

    for cmd in list_cmd_lot1:
        popen = subprocess.Popen(cmd)
        list_popen.append(popen)

    time.sleep(5)

    for cmd in list_cmd_lot2:
        popen = subprocess.Popen(cmd)
        list_popen.append(popen)

    boucle_processus = True
    while boucle_processus:
        # Detection si un processus est terminé pour arreter tout le monde
        for index, popen in enumerate(list_popen):
            if popen.poll() is not None:
                del list_popen[index]
                boucle_processus = False
        time.sleep(60)

    if not boucle_processus:
        for popen in list_popen:
            popen.kill()
