import os
import sys
from common.configuration_loader import ConfigurationLoader
from common.logger_config import LoggerConfig


def retrieve_inputs():
    fichier_config = ""
    if 1 < len(sys.argv):
        fichier_config = sys.argv[1]
    dir = os.path.dirname(os.path.realpath(__file__))
    dossier_rsrc = os.path.join(dir,"..","..","..","resources")
    return fichier_config, dossier_rsrc
    

def configure(config_select=None, root_dir=None):
    '''
    :param config_select:

    :return: tuple avec en premier un logger configuré et en second la configuration json lues

    '''
    # LOGGER
    my_logger = LoggerConfig(
        os.path.join(os.path.dirname(__file__), "log"), "lanceur", "DEBUG")
    _logger = my_logger.get_logger()

    _logger.debug("LOGGER activated")

    # CONFIG
    _config_json = None
    if config_select is not None:
        if root_dir is None:
            dossier_config = os.path.join(os.path.dirname(__file__),
                                      "configs")
        else:
            dossier_config = os.path.join(root_dir,
                                      "configs")
                                                  
                                      
        file = config_select
        gestion_config = ConfigurationLoader(
            dossier_config)
        gestion_config.set_configuration_file_name(file)
        _config_json = gestion_config.obtenir_configuration()

        _logger.debug(_config_json)

    return _logger, _config_json

