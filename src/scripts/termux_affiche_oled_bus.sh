#/usr/bin/bash

PORT=8082

ps -ef |grep python3|grep lanceur.py|cut -f 1 -d ' '|xargs kill

# Detect working directory
dossier_android=$HOME/scripts/afficheur_oled_temp/src/python
lCheminTravail=$(cd `dirname $0` && pwd)
dossier_dev=$lCheminTravail/src/python

if [ -d $dossier_android ] ;then
    dossier=$dossier_android
else
    dossier=$dossier_dev
fi

# Detect version of python
var=$(which python3|wc -l)
if [ $var==1 ] ; then
    pyth=python3
else
    pyth=python
fi


export PYTHONPATH=$dossier

cd $dossier
(
    $pyth lanceur.py bus
) | (
    sleep 3
    xdg-open http://0.0.0.0:$PORT/
)

cd -

exit
