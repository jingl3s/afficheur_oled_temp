'''
@author: zorbac

Copyright 2014 zorbac at free.fr
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import time
import os
import sys
import socket
import subprocess
   

if __name__ == '__main__':
    dossier_src = os.getcwd()
    dossier= os.path.dirname(__file__)
    os.chdir(dossier)
    
    print('argv: {}'.format(sys.argv[0]))
    

    hostname = socket.gethostname()
    print(hostname)
    if hostname in "localhost":
        if sys.version.split(" ")[0] in "3.6.1":
            python_cmd ="/data/user/0/org.qpython.qpy3/files/bin/qpython-android5.sh"
        else:
            python_cmd = "python"
    else:
        python_cmd = "python3"

    list_popen = list()
    
    cmd1=[python_cmd, "andro_test.py"]
    cmd2=[python_cmd, "andro_test.py"]
    popen  = subprocess.Popen(cmd1)
    list_popen.append(popen)
    time.sleep(2)
    popen = subprocess.Popen(cmd2)
    list_popen.append(popen)
    popen = subprocess.Popen(cmd2)
    list_popen.append(popen)
    
    boucle_processus = True
    while boucle_processus:
        # Detection si un processus est terminé pour arreter tout le monde
        for index, popen in enumerate(list_popen):
            if popen.poll() is not None:
                del list_popen[index]
                boucle_processus = False
                print("fin det2cte")
        time.sleep(1)
            
    if not boucle_processus:
        for popen in list_popen:
            print('fils')
            popen.kill()
        
    os.chdir(dossier_src)

    print('fin')