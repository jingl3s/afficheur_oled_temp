#!/usr/bin/env python  

def disp_led(PINIR, PINR, gpio):
    gpio.output(PINR, gpio.HIGH) #       gpio.output(PING, gpio.LOW)
    time.sleep(1)
    if gpio.input(PINIR):
        print 'Input was HIGH'
    else:
        print 'Input was LOW'
    gpio.output(PINR, gpio.LOW)
    #      gpio.output(PING, gpio.HIGH)
    time.sleep(1)

""" Setting up two pins for output """  
import RPi.GPIO as gpio  
import time  
PINIR = 18  # this should be 2 on a V2 RPi  
PINR = 23  # 
#PING = 1  # this should be 3 on a V2 RPi  
gpio.setmode(gpio.BCM)  # broadcom mode  
gpio.setup(PINR, gpio.OUT)  # set up red LED pin to OUTput  
gpio.setup(PINIR, gpio.IN) #, pull_up_down=gpio.PUD_UP)  # set up red LED pin to OUTput  

#Make the two LED flash on and off forever
valeur_defaut = gpio.input(PINIR)  
try:
    while True:  
        #disp_led(PINIR, PINR, gpio)
        valeur_lue = gpio.input(PINIR) 
        if valeur_lue != valeur_defaut:
            valeur_defaut = valeur_lue
            print('Input was {0}'.format(valeur_lue))
        
        
#         if valeur_lue:
#             print('Input was HIGH')
#         else:
#             print('Input was LOW')
            
        time.sleep(0.005)
        ##
        
except KeyboardInterrupt:
    gpio.cleanup()
